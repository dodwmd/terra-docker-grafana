FROM ubuntu:latest
MAINTAINER Michael Dodwell <michael@dodwell.us>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get -y --no-install-recommends install \
    libfontconfig \
    ca-certificates \
    wget && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* \
           /tmp/* \
           /var/tmp/*

# Install grafana

ENV GF_PATHS_DATA /var/lib/grafana
ENV GF_PATHS_LOGS /var/log/grafana
ENV GF_PATHS_PLUGINS /var/lib/grafana/plugins
ENV GF_SERVER_DOMAIN docker.hung.ts0.org
ENV GF_SERVER_ROOT_URL %(protocol)s://%(domain)s:%(http_port)s/grafana/
ENV GRAFANA_VERSION 4.0.1-1480694114

RUN wget --no-check-certificate -q -O /tmp/grafana.deb https://grafanarel.s3.amazonaws.com/builds/grafana_${GRAFANA_VERSION}_amd64.deb && \
    dpkg -i /tmp/grafana.deb && \
    rm /tmp/grafana.deb && \
    chown -R grafana:grafana "$GF_PATHS_DATA" "$GF_PATHS_LOGS" && \
    chown -R grafana:grafana /etc/grafana && \
    rm -rf /var/lib/apt/lists/* \
           /tmp/* \
           /var/tmp/*

VOLUME ["/var/lib/grafana", "/var/log/grafana", "/etc/grafana"]

EXPOSE 3000

CMD ["/usr/sbin/grafana-server", "--homepath=/usr/share/grafana", "--config=/etc/grafana/grafana.ini"]
